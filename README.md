### Keysight IOC ###

This directory contains the repository of the dockerfile for building an IOC for connecting to a Keysight B2985 electrometer

You need to specify the PV Prefix, the IP address and the port number when running the container

You must use host networking. 

The file RELEASE.local contains the names of each of the support modules and their locations.

